# Distributional Clauses (beta) #
Probabilistic logic language for inference, planning and learning in static and dynamic domains  
**DC**: Distributional Clauses for inference in static models.  
**DCPF**: Distributional Clauses Particle Filter for filtering in dynamic models  
**HYPE**: planner for hybrid MDPs based on DCPF.  

The code is in beta, if you need help or find a bug please contact  pedro (_DOT_) zuidbergdosmartires (AT) cs (_DOT_) kuleuven (_DOT_) be

## Prerequisites to install YAP Prolog ##
Make sure the `readline` library is installed properly:

* Ubuntu 16.04 and lower:
```
sudo apt-get install libreadline6 libreadline6-dev
sudo apt-get install libncurses5-dev
```

* Ubuntu 18.04:
```
sudo apt-get install libreadline-dev
sudo apt-get install libncurses5-dev
```

Make sure you compile yap under gcc/g++ compilers with version <=4.9:
```
g++ --version && gcc --version
```
If you have version >4.9 install an older versions
```
sudo apt-get install g++-4.9
sudo apt-get install gcc-4.9
```

For Ubuntu 18.04 you first need to add `xenial` to your `sources.list`:
```
echo "deb http://dk.archive.ubuntu.com/ubuntu/ xenial main" | sudo tee -a /etc/apt/sources.list
echo "deb http://dk.archive.ubuntu.com/ubuntu/ xenial universe" | sudo tee -a /etc/apt/sources.list
sudo apt update
sudo apt install g++-4.9
sudo apt-get install gcc-4.9
```

Use ` update-alternatives` to temporarily change to them. Replace the `X` in the command below with your current verion.
```
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-X 100 --slave /usr/bin/g++ g++ /usr/bin/g++-X
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.9 50 --slave /usr/bin/g++ g++ /usr/bin/g++-4.9
```

Now you can easliy switch between version with:
```
sudo update-alternatives --config gcc
```
Change back to your original version once you have installed YAP.


## Install YAP ##

Download [YAP 6.2.2](https://bitbucket.org/problog/dc_problog/downloads/yap-6.2.2.zip) and follow the instructions below (make sure you are using gcc/g++ compilers with version <=4.9).
```
unzip yap-6.2.2.zip
cd yap-6.2.2
mkdir arch
cd arch
../configure --enable-tabling=yes --enable-dynamic-loading --disable-myddas
make
sudo make install
sudo make install_library
```


**SWITCH** now **BACK** to your original compiler versions!
```
sudo update-alternatives --config gcc
```


## Prerequisites to compile Distributional Clauses
Install the GSL library:

* Ubuntu 14.04:
```
sudo apt-get install libgsl0-dev libgsl0ldbl
```
* Ubuntu 16.04+:
```
sudo apt-get install libgsl2-dev libgsl2:i386
```

* Ubuntu 18.04:
```
sudo apt-get install libgsl-dev
```

Install the boost library:
```
sudo apt-get install libboost-all-dev
```

## Install Distributional Clauses
Clone repository to your home directory and compile dc_problog:
```
git clone https://<username>@bitbucket.org/problog/dc_problog.git
cd dc_problog
sh make.sh
```
Add dc_problog to the YAP library path by adding the following line to your `.bashrc` file.
```
#distributional clauses
export YAPSHAREDIR="/path/to/home/dc_problog/dc_problog"
```
Insert the path to your home directory (or to where you alternatively installed dc_problog).


## Test
Execute
```
sh test.sh
```

Expected output (the error values may differ):
```
Testing test/test1.pl...
[e(1,2)~=true,e(2,3)~=true,e(3,4)~=true]


Testing test/test2.pl...
test1:
Absolute error drawn(1) ~= 1: 0.00397515873015886
Absolute error drawn(1) ~= 2: 0.000566865079369094
Absolute error drawn(1) ~= 3: 0.000907182539679613
Absolute error average g ~ Gaussian(0,0.1): 0.0021772409223738

test2:
Absolute error average g ~ Gaussian(0,0.1): 0.000607731688310789
Absolute error g<0.0: 0.00560000000000005
Absolute error 0.0<g<0.1: 0.00884499999999999
Absolute error g<0.1 | g>0.0: 0.00294085980173986
Absolute error density g=0.0: 3.98792110445356e-13
Absolute error density g2=0.0: 0.0096499337103606
Absolute error density g2=0.1|g=0.1: 1.90070181815827e-13

% 0.388 CPU in 0.387 seconds (100% CPU)
```
## Introduction to DC
DC is based on Yap Prolog. This means that model and inference are written in Prolog.

### Initialization
The first part of the program regards library inclusion and initialization. For example:
```
:- use_module(library(distributionalclause)). % load distributional clauses library
:- use_module(library(lists)). % load list library (prolog)

:- set_options(default),set_query_propagation(true). % options
:- initialization(init). % initialize DC
```
### Options
Set default options (mandatory): ```:- set_options(default).```

Set default options + query propagation ```:- set_options(default),set_query_propagation(true).```

### Model
The model is described using distributional clauses. The syntax is explained in the next section.

Example:
```
% define a discrete random variable (bernoulli)
coin ~ finite([0.2:true,0.8:false]). % syntax name_variable ~ finite([prob:value,prob:value,...])
```

### Inference
Inference is called with the predicate ```query```

Syntax: ``` query(positive_evidence_List,negative_evidence_List,query_to_consider,num_samples,Probability). ```

```eval_query```: same as query

Example:
Given the file model.pl
```
:- use_module(library(distributionalclause)). % load distributional clauses library
:- use_module(library(lists)). % load list library (prolog)
:- set_options(default). % to enable query propagation replace the line with :- set_options(default),set_query_propagation(true).

:- initialization(init). % initialize DC

% define a discrete random variable (bernoulli)
coin ~ finite([0.2:true,0.8:false]). % syntax name_variable ~ finite([prob:value,prob:value,...])
```

call yap -l model.pl and write in the prompt:
```
query([],[],coin ~= true,100,P). % 100 samples
```

Output

```
P = 0.2
```


# DC Syntax


**Built-in Yap-Prolog operators**

The following operators/predicates can be used in DC clauses:

* `=`, `<`, `>`, `<=`, `>=` : numerical comparisons between numbers (not random variables)
* `=` : equality operator (unification)
* `is` : equality operator with expression evaluation

 For example, `10 is 2*5` is true, `A is 2*5` is true for `A=10` (the logical variable A is unified with 10 if possible)

* `between(A,B,C)` : integer number comparison  C is between A and B and C is integer.

 For example, the formula `between(1,5,C)` is true for C=1, 2, 3, 4, 5.
* `%` : single-line comments

**Custom built-in operators/predicates**

`builtin(A)` : set predicate A as built-in. The predicates that unifies with A can be then used inside DC clauses.
  For example,
  ```
  builtin(pos(X)). % any predicate that unifies with pos(X) can be used inside DC clauses
  pos(A) :- A>0. % Prolog clause
  ```

## Deterministic clauses

Syntax: `head <- body.`

explanation: body implies head as clauses in Prolog, the only difference is the implication operator <- instead of :-
The body is a list of literals (atomic formulas or their negations).

Example:
```
not_empty(Y) <- inside(X,Y). % not_empty(Y) is true if there exists an object X such that inside(X,Y) is true.

inside(1,2) <- true. % inside(1,2) is a true fact. The body has to be specified to distinguish it with Prolog syntax
```


## Distributional clauses

Syntax: `head ~ distribution <- body.`

explanation: if 'body' is true the random variable head is defined with a given distribution.

Multiple DC clauses can define the same random variable, as long as the bodies are mutually exclusive.

If none of the bodies that define head is true, the random variable head is not defined.

Examples:
```
coin ~ finite([0.2:true,0.8:false]). % the body is implicitly true.
coin2 ~ finite([0.4:true,0.6:false]) <- coin ~= true. % if coin is true then coin2 is defined with a given distribution.
```

## Operators


**Equality operator for random variables**

Syntax: `var ~= term`

explanation: the value of the random variable 'var' is equal to term (unifiable). A term can be a constant, logical variable or a compund term.

Examples:
```
tcoin <- coin ~= true. % 'tcoin' is true if the value of coin is the constant term 'true'
anyvalue <- coin ~= A. % 'anyvalue' is true if the random variable coin exists (whatever is its value). More formally, anyvalue is true iff exists A such that the value of coin is equal to A.
samevalue <- coin ~= A, coin2 ~= A. % 'samevalue' is true if the values of coin and coin2 are the same. More formally, 'samevalue' is true iff exists A such that the value of coin and coin2 are both equal to A.
```

**Other comparison operators**

At the moment the operators <, >, <=, >= are defined only between constants (not random variables).

To compare the value of a random variable the equality operator is used first.

Examples:
```
person(carl) <- true.
person(bob) <- true.
height(X) ~ gaussian(1.65,0.1) <- person(X). % for each person X, height(x) is a random variable with gaussian distribution
gender(X) ~ finite([0.54:female,0.46:male]) <- person(X).

tall(X) <- height(X) ~= H,H>2. % tall(X) is true iff the value of height(X) is greater than 2.
not_tall(X) <- person(X), \+ tall(X). % not_tall(X) is true if X is a not tall person
```
The definition of 'tall(X)' is deterministic, but since it depends on the random variable 'height(X)', 'tall(X)' is implicitely a random variable. The atom `tall(X)` or its negation `\+ tall(X)` can be used as literals in the body of another clause.

### Supported distributions

* Bernoulli/categorical

 syntax ```finite([probability1:value1,...,probabilityn:valuen])```

 example ```finite([0.1:black,0.5:red,0.4:blue])```
* Uniform categorical (discrete)

 syntax ```uniform([value1,value2,...,valuen])```

 example ```uniform([black,red,blue])```

* Gaussian (univariate)

 syntax ```gaussian(mean,variance)```

 example ```gaussian(0,1)```

* Gaussian (multivariate)

 syntax ```gaussian(meanvector,covariance)```

 example ```gaussian([0,1],[1,0.1,0.1,1])``` % the covariance is flatten row by row

* Beta

 syntax ```beta(a,b)```

 example ```beta(1,2)``` % beta distribution with alpha=1 and beta=2




# DC tutorial
A quick DC tutorial is available in the  `tutorial/tutorial.pl` file.
To run the tutorial change to the examples directory and execute:
```
yap -l tutorial.pl
```
then write `test_coin(100).`, followed by enter. Other queries are explained in the file and can similarly be queried.

# DC and Python
There is the possibility to interface DC models with Python code using the [PyDC package](https://github.com/ML-KULeuven/PyDC).
